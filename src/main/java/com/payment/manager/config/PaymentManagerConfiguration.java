package com.payment.manager.config;


import com.payment.manager.payment.PaymentEntity;
import com.payment.manager.user.persistance.UserEntity;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import static org.hibernate.cfg.AvailableSettings.DIALECT;
import static org.hibernate.cfg.AvailableSettings.DRIVER;
import static org.hibernate.cfg.AvailableSettings.PASS;
import static org.hibernate.cfg.AvailableSettings.SHOW_SQL;
import static org.hibernate.cfg.AvailableSettings.URL;
import static org.hibernate.cfg.AvailableSettings.USER;

import java.util.Properties;


@Configuration
@ComponentScan("com.payment.manager")
@PropertySource("/application-dev.properties")

public class PaymentManagerConfiguration {

  @Bean
  public static SessionFactory getSessionFactory(@Value("${db.url}") String dbUrl,
                                                 @Value("${db.username}") String dbUsername,
                                                 @Value("${db.password}") String dbPassword) {
    try {
      org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration();
      Properties settings = new Properties();
      settings.put(DRIVER, "org.postgresql.Driver");
      settings.put(URL, dbUrl + "?useSSL=false");
      settings.put(USER, dbUsername);
      settings.put(PASS, dbPassword);
      settings.put(DIALECT, "org.hibernate.dialect.MySQL5Dialect");
      settings.put(SHOW_SQL, "true");
      configuration.setProperties(settings);
      configuration.addAnnotatedClass(UserEntity.class);
      configuration.addAnnotatedClass(PaymentEntity.class);
//      configuration.addAnnotatedClass(GroupEntity.class);
//      configuration.addAnnotatedClass(AddressEntity.class);
      ServiceRegistry serviceRegistry =
              new StandardServiceRegistryBuilder().applySettings(configuration.getProperties())
                      .build();
      return configuration.buildSessionFactory(serviceRegistry);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
//  @Bean
//  public DataSource dataSource(@Value("${db.url}") String dbUrl,
//                               @Value("${db.username}") String dbUsername,
//                               @Value("${db.password}") String dbPassword) {
//    return new DataSource(dbUrl, dbUsername, dbPassword);
//  }
//
//  public static class DataSource {
//    private javax.sql.DataSource dataSource;
//
//    private final String dbUrl;
//    private final String dbUsername;
//    private final String dbPassword;
//
//    public DataSource(String dbUrl, String dbUsername, String dbPassword) {
//      this.dbUrl = dbUrl;
//      this.dbUsername = dbUsername;
//      this.dbPassword = dbPassword;
//    }
//
//    public Connection getConnection() throws SQLException {
//      if (dataSource == null) {
//        HikariConfig config = new HikariConfig();
//
//        config.setJdbcUrl(dbUrl);
//        config.setUsername(dbUsername);
//        config.setPassword(dbPassword);
//
//        config.setMaximumPoolSize(20);
//        config.setAutoCommit(true);
//        config.addDataSourceProperty("cachePrepStmts", "true");
//
//        dataSource = new HikariDataSource(config);
//      }
//      return dataSource.getConnection();
//    }
//  }
}
