package com.payment.manager.payment;

import com.payment.manager.exception.DataSourceException;
import com.payment.manager.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentService {

    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public PaymentModel createPayment(PaymentModel model) throws DataSourceException {

        PaymentEntity paymentEntity = mapToEntity(model);

        PaymentEntity storedEntity = paymentRepository.savePayment(paymentEntity);

        return mapToModel(storedEntity);
    }

    public PaymentModel updatePayment(PaymentModel model, Long id) throws DataSourceException {
        PaymentEntity paymentEntity = new PaymentEntity();

        paymentEntity.setId(model.getId());
        paymentEntity.setDate(model.getDate());
        paymentEntity.setTitle(model.getTitle());
        paymentEntity.setPrice(model.getPrice());
        paymentEntity.setUserId(model.getUserId());

        PaymentEntity updatedPayment = paymentRepository.updatePayment(paymentEntity, id);

        return mapToModel(updatedPayment);
    }

    public PaymentModel getPaymentById(Long id) throws DataSourceException, EntityNotFoundException {

        PaymentEntity paymentById = paymentRepository.getPaymentById(id);

        return mapToModel(paymentById);
    }

    public void deletePaymentById(Long id) throws DataSourceException {

        paymentRepository.deletePaymentById(id);
    }

    private PaymentEntity mapToEntity(PaymentModel model) {
        PaymentEntity paymentEntity = new PaymentEntity();

        paymentEntity.setId(model.getId());
        paymentEntity.setDate(model.getDate());
        paymentEntity.setTitle(model.getTitle());
        paymentEntity.setPrice(model.getPrice());
        paymentEntity.setUserId(model.getUserId());

        return paymentEntity;
    }

    private PaymentModel mapToModel(PaymentEntity entity) {
        PaymentModel paymentModel = new PaymentModel();

        paymentModel.setId(entity.getId());
        paymentModel.setDate(entity.getDate());
        paymentModel.setTitle(entity.getTitle());
        paymentModel.setPrice(entity.getPrice());
        paymentModel.setUserId(entity.getUserId());

        return paymentModel;
    }
}
