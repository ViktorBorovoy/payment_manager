package com.payment.manager.payment;

import com.payment.manager.user.persistance.UserEntity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "app_payment")
public class PaymentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "date")
    private Date date;
    @Column(name = "title")
    private String title;
    @Column(name = "price")
    private double price;
    @Column(name = "userId")
    private Long userId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PaymentEntity{" +
                "id=" + id +
                ", date=" + date +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", userId=" + userId +
                '}';
    }
}
