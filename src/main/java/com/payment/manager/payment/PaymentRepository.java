package com.payment.manager.payment;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PaymentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public PaymentEntity savePayment(PaymentEntity paymentEntity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        PaymentEntity entity = (PaymentEntity) session.save(paymentEntity);

        transaction.commit();

        return entity;
    }

    public PaymentEntity updatePayment(PaymentEntity paymentEntity, Long id) {

        paymentEntity.setId(id);

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.saveOrUpdate(paymentEntity);

        transaction.commit();

        return paymentEntity;
    }

    public PaymentEntity getPaymentById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        PaymentEntity entity = session.get(PaymentEntity.class, id);

        transaction.commit();

        return entity;
    }

    public void deletePaymentById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        PaymentEntity paymentEntity = session.get(PaymentEntity.class, id);

        session.delete(paymentEntity);

        transaction.commit();
    }
}
