package com.payment.manager.user.web;


import com.payment.manager.exception.DataSourceException;
import com.payment.manager.exception.EntityNotFoundException;
import com.payment.manager.payment.PaymentModel;
import com.payment.manager.payment.PaymentService;
import com.payment.manager.user.persistance.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/payments")
public class PaymentController {
    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) { this.paymentService = paymentService; }

    @ResponseBody
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentDto> createPayment(@RequestBody CreatePaymentDto createPaymentDto) {
        PaymentModel model = mapToModel(createPaymentDto);

        try {
            PaymentModel paymentModel = paymentService.createPayment(model);

            PaymentDto paymentDto = mapToDto(paymentModel);

            return ResponseEntity.ok(paymentDto);
        } catch (DataSourceException exception) {
            exception.printStackTrace();

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @ResponseBody
    @PutMapping(path = "{paymentId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentDto> updatePayment(@PathVariable Long paymentId, @RequestBody CreatePaymentDto dto) {
        PaymentModel model = mapToModel(dto);

        try {
            PaymentModel paymentModel = paymentService.updatePayment(model, paymentId);

            PaymentDto paymentDto = mapToDto(paymentModel);

            return ResponseEntity.ok(paymentDto);
        } catch (DataSourceException exception) {
            exception.printStackTrace();

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @ResponseBody
    @GetMapping(path = "{paymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentDto> getPaymentById(@PathVariable Long paymentId) {

        try {
            PaymentModel paymentModel = paymentService.getPaymentById(paymentId);

            PaymentDto paymentDto = mapToDto(paymentModel);

            return ResponseEntity.ok(paymentDto);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (DataSourceException e) {
            return ResponseEntity.internalServerError().build();
        }
    }

    @DeleteMapping(path = "{paymentId}")
    public ResponseEntity<Void> deletePaymentById(@PathVariable Long paymentId) {
        try {
            paymentService.deletePaymentById(paymentId);
            return ResponseEntity.noContent().build();
        } catch (DataSourceException e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }

    private PaymentModel mapToModel(CreatePaymentDto dto) {
        PaymentModel model = new PaymentModel();
        model.setDate(dto.getDate());
        model.setTitle(dto.getTitle());
        model.setPrice(dto.getPrice());
        model.setUserId(dto.getUserId());

        return model;
    }

    private PaymentDto mapToDto(PaymentModel model) {
        PaymentDto dto = new PaymentDto();
        dto.setId(model.getId());
        dto.setDate(model.getDate());
        dto.setTitle(model.getTitle());
        dto.setPrice(model.getPrice());
        dto.setUserId(model.getUserId());

        return dto;
    }
}
