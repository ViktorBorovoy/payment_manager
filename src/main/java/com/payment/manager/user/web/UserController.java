package com.payment.manager.user.web;

import com.payment.manager.exception.DataSourceException;
import com.payment.manager.exception.EntityNotFoundException;
import com.payment.manager.user.persistance.UserModel;
import com.payment.manager.user.persistance.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/users")

public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ResponseBody
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> createUser(@RequestBody CreateUserDto createUserDto) {
        UserModel model = mapToModel(createUserDto);

        try {
            UserModel userModel = userService.createUser(model);

            UserDto userDto = mapToDto(userModel);

            return ResponseEntity.ok(userDto);
        } catch (DataSourceException exception) {
            exception.printStackTrace();

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @ResponseBody
    @PutMapping(path = "{userId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> updateUser(@PathVariable Long userId, @RequestBody CreateUserDto dto) {
        UserModel model = mapToModel(dto);

        try {
            UserModel userModel = userService.updateUser(model, userId);

            UserDto userDto = mapToDto(userModel);

            return ResponseEntity.ok(userDto);
        } catch (DataSourceException exception) {
            exception.printStackTrace();

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @ResponseBody
    @GetMapping(path = "{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getUserById(@PathVariable Long userId) {

        try {
            UserModel userModel = userService.getUserById(userId);

            UserDto userDto = mapToDto(userModel);

            return ResponseEntity.ok(userDto);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (DataSourceException e) {
            return ResponseEntity.internalServerError().build();
        }
    }

    @ResponseBody
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDto>> findUserByName(@RequestParam String name) {

        try {
            List<UserModel> userModels = userService.findUserByName(name);
            List<UserDto> userDtoList = new ArrayList<>();
            for (UserModel model : userModels) {
                UserDto userDto = mapToDto(model);
                userDtoList.add(userDto);
            }
            return ResponseEntity.ok(userDtoList);

        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (DataSourceException e) {
            return ResponseEntity.internalServerError().build();
        }
    }


    @DeleteMapping(path = "{userId}")
    public ResponseEntity<Void> deleteUserById(@PathVariable Long userId) {
        try {
            userService.deleteUserById(userId);
            return ResponseEntity.noContent().build();
        } catch (DataSourceException e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().build();
        }
    }


    private UserModel mapToModel(CreateUserDto dto) {
        UserModel model = new UserModel();
        model.setName(dto.getName());
        model.setEmail(dto.getEmail());
        model.setPhone(dto.getPhone());
        model.setActive(dto.getActive());
        model.setRating(dto.getRating());

        return model;
    }

    private UserDto mapToDto(UserModel model) {
        UserDto dto = new UserDto();
        dto.setId(model.getId());
        dto.setName(model.getName());
        dto.setEmail(model.getEmail());
        dto.setPhone(model.getPhone());
        dto.setActive(model.getActive());
        dto.setRating(model.getRating());
        dto.setPayments(model.getPayments());

        return dto;
    }
}
