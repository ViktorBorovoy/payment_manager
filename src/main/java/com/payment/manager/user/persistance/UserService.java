package com.payment.manager.user.persistance;

import com.payment.manager.exception.DataSourceException;
import com.payment.manager.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserModel createUser(UserModel model) throws DataSourceException {

        UserEntity userEntity = mapToEntity(model);

        UserEntity storedEntity = userRepository.saveUser(userEntity);

        return mapToModel(storedEntity);
    }

    private UserEntity mapToEntity(UserModel model) {
        UserEntity userEntity = new UserEntity();

        userEntity.setName(model.getName());
        userEntity.setPhone(model.getPhone());
        userEntity.setEmail(model.getEmail());
        userEntity.setActive(model.getActive());
        userEntity.setRating(model.getRating());
        userEntity.setPayments(model.getPayments());

        return userEntity;
    }

    private UserModel mapToModel(UserEntity entity) {
        UserModel userModel = new UserModel();

        userModel.setId(entity.getId());
        userModel.setName(entity.getName());
        userModel.setPhone(entity.getPhone());
        userModel.setEmail(entity.getEmail());
        userModel.setActive(entity.getActive());
        userModel.setRating(entity.getRating());
        userModel.setPayments(entity.getPayments());

        return userModel;
    }

    public UserModel updateUser(UserModel model, Long id) throws DataSourceException {
        UserEntity userEntity = new UserEntity();

        userEntity.setName(model.getName());
        userEntity.setPhone(model.getPhone());
        userEntity.setEmail(model.getEmail());
        userEntity.setActive(model.getActive());
        userEntity.setRating(model.getRating());

        userRepository.updateUser(userEntity, id);

        return model;
    }

    public UserModel getUserById(Long id) throws EntityNotFoundException, DataSourceException {

        UserEntity userById = userRepository.getUserById(id);

        return mapToModel(userById);
    }
    public void deleteUserById(Long id) throws DataSourceException {

        userRepository.deleteUserById(id);
    }
    public List<UserModel> findUserByName(String name) throws EntityNotFoundException, DataSourceException {

//        List<UserEntity> userByName = userRepository.findUserByName(name);
//        List<UserModel> models = new ArrayList<>();
//
//        for (UserEntity entity : userByName) {
//            UserModel userModel = mapToModel(entity);
//            models.add(userModel);
//        }

        List<UserModel> list = new ArrayList<>();

        return list;
    }
}
