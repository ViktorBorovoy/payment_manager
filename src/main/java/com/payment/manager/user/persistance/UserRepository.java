package com.payment.manager.user.persistance;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public UserEntity saveUser(UserEntity userEntity) {
        return executeWithinSession(session -> (UserEntity) session.save(userEntity));
    }

    private <T> T executeWithinSession(Function<Session, T> function) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        T entity = function.apply(session);

        transaction.commit();

        return entity;
    }

    public UserEntity updateUser(UserEntity userEntity, Long id) {
        userEntity.setId(id);

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.saveOrUpdate(userEntity);

        transaction.commit();

        return userEntity;
    }

    public UserEntity getUserById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        UserEntity entity = session.get(UserEntity.class, id);

        transaction.commit();

        return entity;
    }

    public void deleteUserById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        UserEntity userEntity = session.get(UserEntity.class, id);

        session.delete(userEntity);

        transaction.commit();
    }

//    public List<UserEntity> findUserByName(String name) throws DataSourceException {
//
//        try (Connection connection = dataSource.getConnection();
//             PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_NAME)) {
//
//            preparedStatement.setString(1, name);
//
//            List<UserEntity> userEntityList = new ArrayList<>();
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                UserEntity userEntity = new UserEntity();
//                userEntity.setId(resultSet.getLong("id"));
//                userEntity.setName(resultSet.getString("name"));
//                userEntity.setEmail(resultSet.getString("email"));
//                userEntity.setPassword(resultSet.getString("password"));
//
//                userEntityList.add(userEntity);
//            }
//            return userEntityList;
//
//        } catch (SQLException ex) {
//            throw new DataSourceException("Find user by name error", ex);
//        }
//    }
}

