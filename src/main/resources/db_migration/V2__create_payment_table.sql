create table app_payment (
    id serial,
    'date' date,
    title varchar(1024),
    price double precision,
    userId bigint
)